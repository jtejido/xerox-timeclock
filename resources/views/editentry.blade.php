@extends('master')

@section('title', 'Edit Time Entry')

@section('content')
<div class="page-header">
  <h1>Edit <small>Time Information</small></h1>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="table-responsive">
      <form class="form-inline" method="POST">
        <p class="lead">You can make changes to your entries from here. All changes are recorded.</p>
        <div class="alert alert-danger" role="alert"><b>Heads up!</b> Please enter your time either as HH:MM with either 24 hour clock or 12 hour clock and AM/PM. Example: 2:35 PM or 14:34 are both accepted.</div>

        <table class="table table-bordered table-hover table-scrolly">
          <thead>
            <th>Date</th>
            <th>In</th>
            <th>Out</th>
            <th>Hours</th>
          </thead>
          <tbody>
            <tr>
              <td>{{ $entry->date }}</td>
              <td><input type="text" class="input-sm form-control" name="start_time" value="{{ $entry->punch_in }}"/></td>
              <td><input type="text" class="input-sm form-control" name="end_time" value="{{ $entry->punch_out }}"/></td>
              <td>{{ $entry->hours or 'Not Complete' }}</td>
            </tr>
          </tbody>
        </table>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-success">Update</button>
      </form>
    </div>
  </div>
</div>
@endsection
