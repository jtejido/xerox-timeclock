@extends('master')

@section('title', 'Login')

@section('content')
<div class="row">
  <div class="col-md-offset-4 col-md-4">
    <div class="page-header">
      <h1>Timeclock</h1>
    </div>
    <p>Login using your S3 / Infobank account.</p>
<form action="login" method="POST">
  <div class="form-group">
    <label for="form_input_winid">WIN ID</label>
    <input type="text" class="form-control" id="form_input_winid" name="username" placeholder="WIN ID">
  </div>
  <div class="form-group">
    <label for="form_input_password">Password</label>
    <input type="password" class="form-control" id="form_input_password" name="password" placeholder="Password">
  </div>
  {{ csrf_field() }}
  <button type="submit" class="btn btn-success">Login</button>
</form>
</div>
</div>
@endsection
