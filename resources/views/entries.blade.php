@extends('master')

@section('title', 'Entries')

@section('content')
<div class="page-header">
  <h1>Entries <small>Historical Time Information</small></h1>
</div>

<div class="row">
  <div class="col-md-8">
    <form class="form-inline" action="entries" method="POST">
      <div class="input-daterange input-group" id="datepicker" style="width: auto; !important">
          <input type="text" class="input-sm form-control" name="start_dte" />
          <span class="input-group-addon">to</span>
          <input type="text" class="input-sm form-control" name="end_dte" />
      </div>
      <button type="submit" class="btn btn-info">Search</button>
      {{ csrf_field() }}
    </form>
  </div>
</div>
<br/>
<div class="row">
  <div class="col-md-12">
    <p><a class="btn btn-default" href="{{ route('newPunch') }}"><span class="glyphicon glyphicon-plus"></span> New</a></p>
    @if(count($entries) < 1)
    <div class="alert alert-danger" role="alert"><b>No Records.</b> Your search returned no records.</div>
  @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-scrolly">
        <thead>
          <th style="width: 200px;"></th>
          <th>Date</th>
          <th>In</th>
          <th>Out</th>
          <th>Hours</th>
        </thead>
        <tbody>
          @foreach($entries as $entry)
          <tr>
            <td>
              <a class="btn btn-warning" href="{{ route('editPunch', ['id' => $entry->id]) }}"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
              <a class="btn btn-danger" href="{{ route('deletePunch', ['id' => $entry->id]) }}"><span class="glyphicon glyphicon-trash"></span> Delete</a>
            </td>
            <td>{{ $entry->date }}</td>
            <td>{{ $entry->punch_in or 'Missing' }}</td>
            <td>{{ $entry->punch_out or 'Missing' }}</td>
            <td>{{ $entry->hours or 'Not Complete' }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('pagejs')
  <script>
  $('.input-daterange').datepicker({ format: "yyyy-mm-dd" });
  </script>
@endsection
