@extends('master')

@section('title', 'Edit Time Entry')

@section('content')
<div class="page-header">
  <h1>New <small>Time Information</small></h1>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="table-responsive">
      <form class="form-inline" method="POST">
        <p class="lead">You can add additional entries to your timeclock here.</p>

        <div class="alert alert-danger" role="alert"><b>Heads up!</b> Please enter your time either as HH:MM with either 24 hour clock or 12 hour clock and AM/PM. Example: 2:35 PM or 14:34 are both accepted.</div>

        <table class="table table-bordered table-hover table-scrolly">
          <thead>
            <th>Date</th>
            <th>In</th>
            <th>Out</th>
          </thead>
          <tbody>
            <tr>
              <td><input type="text" class="input-sm form-control datepicker" name="date" value=""/></td>
              <td><input type="text" class="input-sm form-control" name="start_time" value=""/></td>
              <td><input type="text" class="input-sm form-control" name="end_time" value=""/></td>
            </tr>
          </tbody>
        </table>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-success">Add</button>
      </form>
    </div>
  </div>
</div>
@endsection

@section('pagejs')
<script>
$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    endDate: '1d',
});
</script>
@endsection
