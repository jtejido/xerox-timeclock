<nav class="navbar navbar-default">
<div class="container-fluid">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="home">Timeclock</a>
  </div>
  <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
      @if(Auth::check())
      <li class="{{ Request::is('home') ? 'active' : '' }}"><a href="{{ route('home') }}">Home</a></li>
      <li class="{{ Request::is('entries') ? 'active' : '' }}"><a href="{{ route('entries') }}">Entries</a></li>
      <li class="{{ Request::is('history') ? 'active' : '' }}"><a href="{{ route('history') }}">History</a></li>
    @endif
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="http://mystats.services.xerox.com/support">Help</a></li>
      @if(Auth::check())
      <li><a href="{{ route('logout') }}">Logout</a></li>
    @endif
    </ul>
  </div><!--/.nav-collapse -->
</div><!--/.container-fluid -->
</nav>
