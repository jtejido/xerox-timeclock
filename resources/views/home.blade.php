@extends('master')

@section('title', 'Home')

@section('content')
<div class="alert alert-danger" role="alert"><b>Disclaimer</b> This is not your Payroll system. Entries in this system will not change your wages. If you are punching using FEPS/Kronos, please continue to do so until instructed by HR otherwise.</div>

<div class="page-header">
  <h1>Timeclock <small>Time Registration and Review</small></h1>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">Timeclock</div>
      <div class="panel-body">
        <p>You are currently <b>{{ $punch_status }}</b>.</p>

        @if(count($punches) == 0)
        <p>There are no punches for you for today.</p>
          @else
            <table class="table">
              <thead>
                <tr>
                <th>Date</th>
                <th>Punch In</th>
                <th>Punch Out</th>
              </tr>
              </thead>
              <tbody>
            @foreach($punches as $punch)
            <tr>
              <td>{{ $punch->date }}</td>
              <td>{{ $punch->punch_in or 'Missing' }}</td>
              <td>{{ $punch->punch_out or 'Missing' }}</td>
            </tr>
            @endforeach
          </tbody>
          </table>
        @endif
        <a href="punch" class="btn btn-success">Punch</a>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">About You</div>
      <div class="panel-body">
        <h4>{{ $user->fullname }}</h4>
        <p><b>{{ $user->jobtitle }}</b> working in <b>{{ $user->location }}</b> on <b>{{ $user->sbu }}</b>.</p>
        <p>You are currently configured for <b>{{ $user->timezone->timezone }}</b>. If this is not correct, please update your timezone below.</p>
        <a href="timezone" class="btn btn-primary">Change Timezone</a>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">Need Help</div>
      <div class="panel-body">
        <p>Timeclock is supported by Data & Performance Management in Customer Care Operations Support. Users using this tool who need assistance can raise a support ticket via our helpdesk.</p>
        <a href="http://mystats.services.xerox.com/support/" class="btn btn-primary" target="_blank">Get Help</a>
      </div>
    </div>
  </div>
</div>
@endsection
