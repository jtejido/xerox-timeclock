@extends('master')

@section('title', 'Update Time Zone')

@section('content')
<div class="page-header">
  <h1>Time Zone <small>Configure your Time Zone</small></h1>
</div>

<div class="row">
  <div class="col-md-12">
    <form action="timezone" method="POST">
      <div class="form-group">
        <label for="form_input_timezone">Time Zone</label>
        {!! Form::select('timezone', $timezones, $active_timezone, array('id' => 'form_input_timezone')) !!}
      </div>
      {{ csrf_field() }}
      <button type="submit" class="btn btn-success">Update Time Zone</button>
    </form>
  </div>
</div>
@endsection
