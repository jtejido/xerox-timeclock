@extends('master')

@section('title', 'History')

@section('content')
<div class="page-header">
  <h1>History <small>Changes to your Timeclock</small></h1>
</div>

<div class="row">
  <div class="col-md-12">
    <p class="lead">All changes listed are manual edits to your timeclock.</p>

    @if(count($entries) < 1)
      <div class="alert alert-danger" role="alert"><b>No Records.</b> There are no audit results.</div>
    @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-scrolly">
        <thead>
          <th>Changed On (GMT)</th>
          <th>Date</th>
          <th>Item</th>
          <th>Previous</th>
          <th>New</th>
        </thead>
        <tbody>
          @foreach($entries as $entry)
          <tr>
            <td>{{ $entry->created_at }}</td>
            <td>{{ $entry->punch->date or 'Deleted Punch' }}</td>
            <td>{{ $entry->changed }}</td>
            <td>{{ $entry->previous }}</td>
            <td>{{ $entry->new }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
