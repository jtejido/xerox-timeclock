<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Punch extends Model
{

  use SoftDeletes;

  protected $dates = ['deleted_at'];

  public function timezone() {
    return $this->hasOne('App\Timezone', 'id', 'timezone_id');
  }
}
