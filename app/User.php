<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public function timezone() {
      return $this->hasOne('App\Timezone', 'id', 'timezone_id');
    }

}
