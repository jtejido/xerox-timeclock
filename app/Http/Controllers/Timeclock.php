<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use Session, View, DB, Input, Redirect, Auth;
use \App\User;
use \App\Punch;
use \App\Audit;
use Carbon\Carbon;
use \App\Http\Library\Timepunch;

class Timeclock extends Controller {

    public function punch()
    {

      Timepunch::punch();

      return redirect('home')->with('status', 'Your punch was recorded.');

    }

    public function entries(Request $request)
    {

      $start_dte = $request->start_dte;

      $end_dte = $request->end_dte;

      if(!empty($start_dte)) {
        $start_dte = Carbon::parse($start_dte);
      }

      if(!empty($end_dte)) {
        $end_dte = Carbon::parse($end_dte);
      }


      $entries = Punch::select()
      ->where('win_id', Auth::user()->win_id);
      
      if((!empty($end_dte)) && (!empty($start_dte))) {
        $entries = $entries->where('date','>=',$start_dte->toDateString());
      }
      if((!empty($end_dte)) && (!empty($start_dte))) {
        $entries = $entries->where('date','<=',$end_dte->toDateString());
      }
      $entries = $entries->get();

      return View::make('entries', compact('entries'));

    }

    public function history(Request $request)
    {

      $entries = [];

      $entries = Audit::where('user_id', Auth::user()->id)->get();

      return View::make('history', compact('entries'));

    }

    public function newEntry(Request $request)
    {

      return View::make('newentry');

    }

    public function saveNewEntry(Request $request)
    {

      Timepunch::newPunch($request->date, $request->start_time, $request->end_time);

      return redirect('entries')->with('status', 'Punch has been added.');

    }

    public function editEntry(Request $request, $timeclockid)
    {

      $entry = Punch::where('win_id', Auth::user()->win_id)->where('id',$timeclockid)->first();

      return View::make('editentry', compact('entry'));

    }

    public function deleteEntry(Request $request, $timeclockid)
    {

      $punch = Punch::where('win_id', Auth::user()->win_id)->where('id',$timeclockid)->first();

      $punch->delete();

      return redirect('entries')->with('status', 'Punch has been deleted.');

    }

    public function save(Request $request, $timeclockid)
    {

      $entry = Punch::where('win_id', Auth::user()->win_id)->where('id',$timeclockid)->first();

      Timepunch::editPunch($entry->id, $request->start_time, $request->end_time);

      return redirect('entries')->with('status', 'Punch has been updated.');

    }
}
