<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use Session, View, DB, Input, Redirect, Auth;
use \App\Timezone;
use \App\Punch;
use Carbon\Carbon;

class TimeConfig extends Controller {

    public function index()
    {

      $timezones = Timezone::all()->pluck('timezone','id')->toArray();

      $active_timezone = Auth::user()->timezone_id;

      return View::make('timezone', compact('timezones', 'active_timezone'));

    }

    public function update(Request $request)
    {

      $timezone = $request->timezone;

      $user = Auth::user();

      $user->timezone_id = $request->timezone;

      $user->save();

      return redirect('home')->with('status', 'Timezone has been updated.');

    }

}
