<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Library\LDAP;
use \App\Http\Controllers\Controller;
use Session, View, DB, Input, Redirect, Auth;
use \App\User;

class Login extends Controller {

    public function __construct(LDAP $ldap) {
      $this->ldap = $ldap;
    }

    public function index()
    {
      return View::make('login');
    }

    public function process(Request $request)
    {
      if(!env('APP_DEBUG', true)) {

        if (!$this->ldap->authenticate($request->input('username'), $request->input('password'), env('LDAP_DOMAIN'), env('LDAP_SERVER'))) {

          return redirect('login')->with('status', 'Username and/or password are incorrect.');

        }

      }

      $user = User::where('win_id', $request->input('username'))->first();

      if(!isset($user)) {

        return redirect('login')->with('status', 'Not a valid user.');

      }

      Auth::login($user);

      return redirect('home')->with('status', 'You are now logged in.');

    }

    public function logout() {

      Auth::logout();

      Session::flush();

      return redirect('login')->with('status', 'You are now logged out.');

    }

    public function home() {

      if(Auth::check()) {

        return redirect('home');

      } else {

        return redirect('login');

      }

    }

}
