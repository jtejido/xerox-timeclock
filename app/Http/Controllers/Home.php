<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use Session, View, DB, Input, Redirect, Auth;
use \App\User;
use \App\Punch;
use Carbon\Carbon;
use \App\Http\Library\Timepunch;

class Home extends Controller {

    public function index()
    {

      $user = Auth::user();

      $carbon = Carbon::now($user->timezone->tzdb);

      $punches = Punch::where('win_id', $user->win_id)->where('date', $carbon->toDateString())->get();

      $punch_status = Timepunch::punchedIn();

      return View::make('home', compact('user', 'punches', 'punch_status'));

    }

}
