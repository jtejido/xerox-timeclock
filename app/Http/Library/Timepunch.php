<?php

namespace App\Http\Library;
use \App\User;
use \App\Punch;
use \App\Audit;
use \App\Timezone;
use Carbon\Carbon;
use Auth;

class Timepunch {

    public static function punch()
    {

      $user = Auth::user();

      $carbon = Carbon::now($user->timezone->tzdb);

      $punch = Punch::where('win_id', $user->win_id)->where('date', $carbon->toDateString())->get()->last();

      if(!$punch or $punch->punch_out) {

        $punch = new Punch;

        $punch->win_id = $user->win_id;

        $punch->timezone_id = $user->timezone_id;

        $punch->date = $carbon->toDateString();

        $punch->punch_in = $carbon->toTimeString();

        $punch->punch_out = null;

        $punch->hours = null;

        $punch->save();

      } elseif(!$punch->punch_out) {

        $carbon = Carbon::now(Timezone::where('id', $punch->timezone_id)->first()->tzdb);

        $punch->punch_out = $carbon->toTimeString();

        $punch_out = Carbon::createFromFormat('Y-m-d H:i:s',$punch->date.' '.$punch->punch_out);

        $punch->hours = $punch_out->diffInSeconds(Carbon::createFromFormat('Y-m-d H:i:s',$punch->date.' '.$punch->punch_in))/3600;

        $punch->save();

      }

      return true;

    }

    public static function editPunch($punch_id, $startTime, $endTime) {

      $user = Auth::user();

      $punch = Punch::where('win_id', $user->win_id)->where('id', $punch_id)->get()->last();

      $punch_in = new Carbon($startTime, $punch->timezone->tzdb);

      $punch_out = new Carbon($endTime, $punch->timezone->tzdb);

      if($punch_in->hour > $punch_out->hour) {

        $punch_out->addDay();

      }

      $punch->punch_out = $punch_out->toTimeString();

      $punch->punch_in = $punch_in->toTimeString();

      $punch->hours = $punch_out->diffInSeconds($punch_in)/3600;

      Timepunch::audit($punch, 'punch_in', $punch->punch_in);

      Timepunch::audit($punch, 'punch_out', $punch->punch_out);

      $punch->save();

    }

    public static function newPunch($date, $startTime, $endTime) {

      $punch = new Punch;

      $user = Auth::user();

      $date = new Carbon($date, $user->timezone->tzdb);

      $punch_in = new Carbon($startTime, $user->timezone->tzdb);

      $punch_out = new Carbon($endTime, $user->timezone->tzdb);

      if($punch_in->hour > $punch_out->hour) {

        $punch_out->addDay();

      }

      $punch->win_id = $user->win_id;

      $punch->timezone_id = $user->timezone_id;

      $punch->date = $date->toDateString();

      $punch->punch_out = $punch_out->toTimeString();

      $punch->punch_in = $punch_in->toTimeString();

      $punch->hours = $punch_out->diffInSeconds($punch_in)/3600;

      $punch->save();

      Timepunch::audit($punch, 'date', $punch->date);

      Timepunch::audit($punch, 'punch_in', $punch->punch_in);

      Timepunch::audit($punch, 'punch_out', $punch->punch_out);

    }
    public static function punchedIn() {

      $user = Auth::user();

      $carbon = Carbon::now($user->timezone->tzdb);

      $punch = Punch::where('win_id', $user->win_id)->where('date', $carbon->toDateString())->get()->last();

      if(!$punch) {

        return "Not Punched In";

      } elseif($punch->punch_out) {

        return "Punched Out";

      } elseif(!$punch->punch_out) {

        return "Punched In";

      } else {

        return "Error State";

      }

    }

    private static function audit($punch, $object, $new) {

      $audit = new Audit;

      $user = Auth::user();

      $audit->mod_user_id = $user->id;

      $audit->user_id = $user->id;

      $audit->timecard_id = $punch->id;

      if($object == "punch_in") {

        $audit->changed = "Punch In";

        $audit->previous = $punch->punch_in;

      } elseif($object == "punch_out") {

        $audit->changed = "Punch Out";

        $audit->previous = $punch->punch_out;

      } elseif($object == "date") {

        $audit->changed = "Date";

        $audit->previous = $punch->date;

      }

      $audit->new = $new;

      $audit->save();

    }

}
