<?php

namespace App\Http\Library;
use Log;

class LDAP {
 public static function authenticate($username, $password, $domain, $connect) {
   if(empty($username) or empty($password)) {
     Log::error('Error binding to LDAP: username or password empty');
     return false;
   }

    $ldapconn = @ldap_connect( $connect ) or die("Could not connect to LDAP server.");
    $result = false;

    if($ldapconn) {
       $ldapbind = @ldap_bind($ldapconn, $username.'@'.$domain, $password);
        if ($ldapbind) {
          $result = true;
        } else {
          Log::error('Error binding to LDAP server '.$connect.' using '.$username.'@'.$domain);
        }
        @ldap_unbind($ldapconn);
      } else {
       Log::error('Error connecting to LDAP.');
     }
     return $result;
 }
}
