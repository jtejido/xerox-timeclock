<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
  public function punch() {
    return $this->hasOne('App\Punch', 'id', 'timecard_id');
  }

}
