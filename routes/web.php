<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'Login@home');
Route::get('login', 'Login@index');
Route::post('login', 'Login@process');
Route::get('logout', 'Login@logout')->name('logout');


Route::group(['middleware' => ['auth']], function () {
  Route::get('home', 'Home@index')->name('home');
  Route::get('timezone', 'TimeConfig@index')->name('timezone');
  Route::post('timezone', 'TimeConfig@update');
  Route::get('punch', 'Timeclock@punch')->name('punch');
  Route::any('entries', 'Timeclock@entries')->name('entries');
  Route::get('history', 'Timeclock@history')->name('history');
  Route::get('new', 'Timeclock@newEntry')->name('newPunch');
  Route::post('new', 'Timeclock@saveNewEntry');
  Route::get('edit/{id}', 'Timeclock@editEntry')->name('editPunch');
  Route::post('edit/{id}', 'Timeclock@save');
  Route::get('delete/{id}', 'Timeclock@deleteEntry')->name('deletePunch');

});
