<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('audits', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->integer('mod_user_id');
          $table->integer('user_id');
          $table->integer('timecard_id');
          $table->string('changed');
          $table->string('previous')->nullable();
          $table->string('new')->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('audits');
    }
}
