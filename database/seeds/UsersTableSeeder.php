<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'win_id' => 20537566,
        'timezone_id' => 1,
        'jobtitle' => 'Sys Dev Principal',
        'fullname' => 'Robert Wright',
        'sbu' => 'CCOS Data & Performance Management',
        'costcenter_id' => '17000000',
        'costcenter' => 'Reporting UK',
        'location' => 'Poole'
      ]);

      DB::table('timezones')->insert([
        'timezone' => 'Eastern Time Zone',
        'tzdb' => 'US/Eastern',
      ]);
      DB::table('timezones')->insert([
        'timezone' => 'Central Time Zone',
        'tzdb' => 'US/Central',
      ]);
      DB::table('timezones')->insert([
        'timezone' => 'Mountain Time Zone',
        'tzdb' => 'US/Mountain',
      ]);
      DB::table('timezones')->insert([
        'timezone' => 'Pacific Time Zone',
        'tzdb' => 'US/Pacific',
      ]);
      DB::table('timezones')->insert([
        'timezone' => 'Alaska Time Zone',
        'tzdb' => 'US/Alaska',
      ]);
      DB::table('timezones')->insert([
        'timezone' => 'Hawaii Time Zone',
        'tzdb' => 'US/Hawaii',
      ]);
      DB::table('timezones')->insert([
        'timezone' => 'Mountain (Arizona) Time Zone',
        'tzdb' => 'US/Arizona',
      ]);
      DB::table('timezones')->insert([
        'timezone' => 'UK Time Zone',
        'tzdb' => 'Europe/London',
      ]);
    }
}
